<?php

function comparison_function(&$a, &$b) {
  return $a <= $b;
}

function _dump($data) {
	if (func_num_args() > 1) {
		$data = func_get_args();
	}
	echo "<pre>".print_r($data, 1)."</pre>";
}

// load array
require "load.php";

// insertion sort
require "insertion_sort2.php";
#_dump($b);
$start_time = microtime(true);
#insertion_sort($b); // Sort the elements
printf("execution time %f  \n", microtime(true) - $start_time);
#_dump($b);

// merge sort
require "merge_sort2.php";
#_dump($a);
$start_time = microtime(true);
#merge_sort($c, 1, sizeof($c)); // Sort the elements
printf("execution time %f  \n", microtime(true) - $start_time);
#_dump($a);

// quick sort
require "quick_sort.php";
#_dump($a);
$start_time = microtime(true);
#quick_sort($a, 0, sizeof($a)-1); // Sort the elements
printf("execution time %f  \n", microtime(true) - $start_time);
#_dump($a);

// radix sort
require "radix_sort.php";
#_dump($b);
$start_time = microtime(true);
radix_sort($b);// Sort the elements
printf("execution time %f  \n", microtime(true) - $start_time);
#_dump($b);

