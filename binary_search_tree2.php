<?php

Class Node{
  public $data = null;
  public $left = null;
  public $right = null;
  public $parent = null;
  
  public function __construct( $data ){
    $this->data = $data;
  }
  
  public function __toString(){
    return $this->data;
  }
}

Class BST{
  protected $_root = null;
  
  protected function _insert( &$new, &$node ){
    if( $node == null){
      $node = $new;
      return;
    }
    if( $new->data <= $node->data ){
      if($node->left == null){
        $node->left = $new;
        $new->parent = $node;
      } else{
        $this->_insert( $new, $node->left ); 
      }
    } else {
      if( $node->right == null ){
        $node->right = $new;
        $new->parent = $node;
      } else {
        $this->_insert( $new, $node->right );
      }
    }
  }
  
  protected function _search( &$target, &$node ){
    if ( $target == $node ){
      return 1;
    } else if( $target->data <= $node->data && isset($node->left) ){
      return $this->_search($target, $node->left);
    } else if( $target->data > $node->data && isset($node->right) ){
      return $this->_search($target, $node->right);
    }
    
    return 0;
  }
  
  public function insert( $node ){
    $this->_insert( $node , $this->_root );
  }
  
  public function search($item){
		return $this->_search($item, $this->_root);
	}
  
}

function _dump($data) {
	if (func_num_args() > 1) {
		$data = func_get_args();
	}
	echo "<pre>".print_r($data, 1)."</pre>";
}

#root
$a = new Node(7);
$BST = new BST;
$BST->insert($a);

#level 1
$b = new Node(4);
$c = new Node(12);
$BST->insert($b);
$BST->insert($c);

_dump($BST);
echo "\n";

$e = new Node(10);
echo $BST->search($c);
echo "\n";
echo $BST->search($e);
echo "\n";





