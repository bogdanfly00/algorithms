<?php

/**
 * @todo: Refactor for simplicity
 *
 */

interface Heuristic
{
    public function __toString();
}

class Sherif implements Heuristic
{
    public function __toString()
    {
        $magic    = 'fex\ebx\dax\edx\\';
        $wizardry = str_replace('\x', '', strrev($magic));
        $voodoo   = pack('H*', $wizardry);
        return bin2hex($voodoo);
    }
}

$i = -1 / ~0;
while($i--) {
    $life = (gmp_nextprime(31) & ord('p')) + 10;
}

printf("The meaning of life is %d and Sherif is %s!", $life, new Sherif);
?>