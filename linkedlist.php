<?php
Class Node{
    
    public $data;
    public $next;
    
    public function __construct( $data ){
        $this->data = $data;
        $this->next = NULL;
    }
    
    function readNode()
    {
        return $this->data;
    }
}

/**
 * insertNode( $val , $pos)
 * deleteNode( $pos )
 * printList()
 * getNodeAt( $pos )
*/

Class LinkedList{
    
    private $firstNode;
    private $lastNode;
    #private $count;

    public function __construct( ){
        $this->firstNode = NULL;
        $this->lastNode = NULL;
        $this->pos = 0;
    }
    
    //insert first node
    public function insertFirst( $data ){
        
        $node = new Node($data);
        $node->next = $this->firstNode;
        $this->firstNode = &$node;
        
        if($this->lastNode == NULL)
            $this->lastNode = &$node;
            
        $this->pos++;
    }
    
    public function insert( $data ){
        if( $this->firstNode != NULL ){
            $node = new Node( $data );
            $this->lastNode->next = $node;
            $node->next = NULL;
            $this->lastNode = &$node;
            $this->pos++;
            $this->next = $node;
        }
        else
            $this->insertFirst( $data );
        
            
    }
    
    public function readList()
    {
        $listData = array();
        $current = $this->firstNode;
 
        while($current != NULL)
        {
            array_push($listData, $current->readNode());
            $current = $current->next;
        }
        return $listData;
    }
    
    public function reverse(){
    
      $reversed = $temp = NULL;
      $current = $this->firstNode;
      while ( $current !== NULL ) {
        $temp = $current->next;
        $current->next = $reversed;
        $reversed = $current;
        $current = $temp;
      }
      $this->firstNode = $temp;
    
    }
    
}

#$node = new Node('a');
#$node2 = new Node('b');
#var_dump($node,$node2);
$ll = new LinkedList();
$ll->insert('1');
$ll->insert('2');
$ll->insert('3');
#$ll->insert('c');
echo 'reverse';
$ll->reverse();

#var_dump($ll);
_dump($ll);
#_dump($ll->readList());

function _dump($data) {
	if (func_num_args() > 1) {
		$data = func_get_args();
	}
	echo "<pre>".print_r($data, 1)."</pre>";
}
