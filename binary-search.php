<?php

$list = array(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144);
$x = 5;
 
// recursive
function binary_search($x, $list, $left, $right){
 if( $left > $right)
  return -1;

  #$mid = floor( ($left+$right)/2);
  $mid = $left+$right >> 1;
  
  if( $list[$mid] == $x ){
    return $mid;
  }
  else if( $x < $list[$mid] ){
    return  binary_search($x, $list, $left, $mid-1);
  }else if( $x > $list[$mid] ){
    return binary_search($x, $list, $mid+1, $right);
  }

}

echo 'recursive = '. binary_search($x, $list, 0, count($list)-1);
echo "\n";

function iterative_binary_search($x, $list) 
{
  $left = 0;
  $right = count($list)-1;
  
  while ($left <= $right){
    
    $mid = $left + $right >> 1;
    
    if( $list[$mid] == $x)
      return $mid;
    elseif( $x < $list[$mid] )
      $right = $mid - 1;
    elseif( $x > $list[$mid])
      $left = $mid + 1;
  
  }
}
  
  

echo 'iterative = '.iterative_binary_search($x, $list, 0, count($list)-1);
echo "\n";

?>