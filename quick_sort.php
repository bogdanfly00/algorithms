<?php
/**
 * Implementation of quick sort sort algorithm in PHP. This code is
 * direct pseudo-code implementation from Cormen et al book, Introduction
 * to Algorithms.
 *
 * Copyright (c) 2012 Akeda Bagus (admin@gedex.web.id).
 *
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * license.
 */

/**
 * Assuming $elements is array with numerical-index starting with zero, that's
 * $p, with upper bound index sizeof($elements) - 1, that's $r.
 * @param reference $elements Reference to array element.
 * @param int $p Starting index.
 * @param int $r Pivot index where each element $p to $r - 1 will be compared
 *               against.
 * @return void
 */
function quick_sort(&$elements, $p, $r, $fn = 'comparison_function') {
  if ($p < $r) {
    ## Lomuto partition scheme  [$p, $partion) .. ($partition, $q]
    #$partition = _partition($elements, $p, $r, $fn);
    #quick_sort($elements, $p, $partition - 1, $fn);
    #quick_sort($elements, $partition + 1, $r, $fn);
    
    ## Hoare partition scheme [$p, $partition] .. ($partition, $q]
    $partition = _partitionH($elements, $p, $r, $fn);
    quick_sort($elements, $p, $partition, $fn);
    quick_sort($elements, $partition + 1, $r, $fn);
  }
}

## Lomuto partition scheme
function _partition(&$elements, $p, $r, $fn) { // $p = hi, $r = lo
  $pivot = $elements[$r]; // set pivot RIGHT element
  $i = $p;
  for ($j = $p; $j <= $r - 1; $j++) {
    if ( $fn($elements[$j], $pivot) ) {
      _swap_element($elements[$i], $elements[$j]);
      $i = $i + 1;
    }
  }
  _swap_element($elements[$i], $elements[$r]);

  return $i;
}


## Hoare partition scheme
function _partitionH(&$elements, $p, $r, $fn){
  $pivot = $elements[$p];
  $i = $p - 1;
  $j = $r + 1;
  while (true){
    do{
      $i++;
    }while( $elements[$i] < $pivot );
    do{
      $j--;
    }while( $elements[$j] > $pivot );
    if ( $i >= $j )
      return $j;
    _swap_element( $elements[$i], $elements[$j]);
  }
}

function _swap_element(&$a, &$b) {
  if ($a != $b) {
    $a ^= $b;
    $b ^= $a;
    $a ^= $b;
  }
}

#function comparison_function(&$a, &$b) {
#  return $a <= $b;
#}
#
#// Example usage:
#$a = array(3, 5, 9, 8, 5, 7, 2, 1, 13);
#var_dump($a);
#quick_sort($a, 0, sizeof($a) - 1); // Sort the elements
#var_dump($a);
