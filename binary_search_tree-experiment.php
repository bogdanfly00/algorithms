<?php

class Node {
    public $info;
    public $left;
    public $right;
    public $level;
    public $parent;
    public function __construct($info) {
      $this->info = $info;
      $this->left = NULL;
      $this->right = NULL;
      $this->level = NULL;
    }
    public function __toString() {
      return "$this->info";
    }
}  


    class BinarySearchTree{
          public $_root;
          public $_sentinel;
          public function  __construct() {
                 $this->root = NULL;
          }
  
          public function create($info) {
              
                 if($this->root == NULL) {
                    $this->root = new Node($info);
                 } else {
  
                    $current = $this->root;
                    while(true) {
                          if($info < $current->info) {
                         
                                if($current->left) {
                                   $current = $current->left;
                                } else {
                                   $current->left = new Node($info);
                                   break; 
                                }
                          } else if($info > $current->info){
                                if($current->right) {
                                   $current = $current->right;
                                } else {
                                   $current->right = new Node($info);
                                   break; 
                                }
                          } else {
                            break;
                          }
                    } 
                 }
          }
          public function traverse($method) {
                 switch($method) {
                     case 'inorder':
                     $this->_inorder($this->root);
                     break;
                     case 'postorder':
                     $this->_postorder($this->root);
                     break;
    
                     case 'preorder':
                     $this->_preorder($this->root);
                     break;
   
                     default:
                     break;
                 } 
          } 
          private function _inorder($node) {
                          if($node->left) {
                             $this->_inorder($node->left); 
                          } 
                          echo $node. " ";
                          if($node->right) {
                             $this->_inorder($node->right); 
                          } 
          }
          private function _preorder($node) {
                          echo $node. " ";
                          if($node->left) {
                             $this->_preorder($node->left); 
                          } 
                          if($node->right) {
                             $this->_preorder($node->right); 
                          } 
          }
          private function _postorder($node) {
                          if($node->left) {
                             $this->_postorder($node->left); 
                          } 
                          if($node->right) {
                             $this->_postorder($node->right); 
                          } 
                          echo $node. " ";
          }
          public function BFT() {
                 $node = $this->root;
                 
                 $node->level = 1; 
                 $queue = array($node);
                 $out = array("<br/> \n");
                 $current_level = $node->level;
  
                 while(count($queue) > 0) {
                       $current_node = array_shift($queue);
                       if($current_node->level > $current_level) {
                            $current_level++;
                            array_push($out,"<br/>\n");  
                       } 
                       array_push($out,$current_node->info. " \t");
                       if($current_node->left) {
                          $current_node->left->level = $current_level + 1;
                          array_push($queue,$current_node->left); 
                       }    
                       if($current_node->right) {
                          $current_node->right->level = $current_level + 1;
                          array_push($queue,$current_node->right); 
                       }    
                 }
    
                
                return join($out,""); 
          }
          
          public function insert($val) {
            if (!is_object($val) || getclass($val) !== 'Node') {
              $newNode = new Node($val);
            } else {
              $newNode = $val;
            }
            $newNode->left = NULL;
            $newNode->right = NULL;
        
            $node = $this->_root;
            $parent = $this->_sentinel;
            while ($node !== $this->_sentinel) {    
              $parent = $node;
              if ($newNode->info < $node->info) {
                $node = $node->left;
              } else {
                $node = $node->right;
              }
            }
        
            if ($parent === $this->_sentinel) {
              $newNode->parent  = $this->_sentinel;
              $this->_root = $newNode;
            } else {
              if ($newNode->info < $parent->info) {
                $parent->left = $newNode;
              } else {
                $parent->right = $newNode;
              }
              $newNode->parent = $parent;
            }
          }
          
          public function search($val) {
            $node = $this->_root;
        
            while ($node !== $this->_sentinel && $node->info !== $val) {
              if ($val < $node->info) {
                $node = $node->left;
              } else {
                $node = $node->right;
              }
            }
            return $node;
          }
}
          
    
  

$BST = new BinarySearchTree;
#root
$BST->insert(7);

#level 1
$BST->insert(4);
$BST->insert(12);

#level 2
$BST->insert(2);
$BST->insert(6);
$BST->insert(9);
$BST->insert(19);

#level 3
#left
$BST->insert(3);
$BST->insert(5);

#right
$BST->insert(8);
$BST->insert(11);
$BST->insert(15);
$BST->insert(20);

function test_searching($num) {
  global $BST;

  $node = $BST->search($num);
  if ($node->info !== NULL) {
    echo "Node with value $num is found\n";
    echo "Parent's node = " . $node->parent . "\n";
    echo "Left-child's node = " . $node->left . "\n";
    echo "Right-child's node = " . $node->right . "\n";
  } else {
    echo "Node with value $num is NOT found\n";
  }
  echo "\n";
}

test_searching(7);
test_searching(4);
test_searching(15);
test_searching(3);
test_searching(2);
#
#test_searching(12);
#test_searching(9);
#test_searching(19);