<?php
/**
 * Implementation of insertion sort algorithm in PHP. This code is
 * direct pseudo-code implementation from Cormen et al book, Introduction
 * to Algorithms.
 *
 * Copyright (c) 2012 Akeda Bagus (admin@gedex.web.id).
 *
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * license.
 */

/**
 * Assuming $elements is array with numerical-index starting with zero
 * and its size greater than 1.
 * @see http://en.wikipedia.org/wiki/Insertion_sort
 * @param reference $elements Reference to array element.
 * @param string $fn Function used as a comparison function.
 * @returns void $elements already sorted in place.
 */
function insertion_sort(&$elements, $fn = 'comparison_function') {
  if (!is_array($elements) || !is_callable($fn)) return;

  for ($i = 1; $i < sizeof($elements); $i++) {
    $key = $elements[$i]; // $a = 1 ; 2
    
    $j = $i-1; // $b = 0 ; 1
    while( $j>=0 && ( $fn($key, $elements[$j]) ) ){
      $elements[$j+1] = $elements[$j]; // 1 == 0 ; 2 == 1
      $j = $j-1; // $j ==-1 ; $j == 0
    }
    $elements[$j + 1 ] = $key; // $j[0] = $key $j[1] = $key

  }
}

/**
 * Comparison function used to compare each element.
 * @param mixed $a
 * @param mixed $b
 * @returns bool True iff $a is less than $b.
 */
#function comparison_function(&$a, &$b) {
#  return $a < $b;
#}


// Example usage:
#$a = array(3, 5, 9, 8, 5, 7, 2, 1, 13, 8);
#var_dump($a);
#insertion_sort($a); // Sort the elements
#var_dump($a);
